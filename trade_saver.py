import sqlite3
import sys
import os
import threading
import time
import urllib2
import json
import socket

btc_usd_url = "https://btc-e.com/api/2/btc_usd/trades"
ltc_btc_url = "https://btc-e.com/api/2/ltc_btc/trades"

urls = ["https://btc-e.com/api/2/btc_usd/trades",
        "https://btc-e.com/api/2/ltc_btc/trades",
        "https://btc-e.com/api/2/ltc_usd/trades",
        "https://btc-e.com/api/2/nmc_btc/trades",
        "https://btc-e.com/api/2/nvc_btc/trades",
        "https://btc-e.com/api/2/trc_btc/trades",
        "https://btc-e.com/api/2/ppc_btc/trades"]

class PrintData(object):
    def btc_usd(self, table):
        btctousd = float(0)
        allTimeBuyCount = 0
        allTimeBuyAvg = 0
        allTimeSellCount = 0
        allTimeSellAvg = 0
        fiveMinBuyCount = 0
        fiveMinBuyAvg = 0
        fiveMinSellCount = 0
        fiveMinSellAvg = 0
        tenMinBuyCount = 0
        tenMinBuyAvg = 0
        tenMinSellCount = 0
        tenMinSellAvg = 0
        conn2 = sqlite3.connect('./.trades.db', timeout=30)
        c2 = conn2.cursor()
        infoArray = c2.execute("SELECT * FROM %s ORDER BY date DESC" % table).fetchall()
        #print infoArray
        for info in infoArray:
            if info[6] == "ask":
                allTimeBuyCount+=1
                allTimeBuyAvg+=info[2]

                if (int(time.time() * 1000) / 1000 - info[0]) < 600:
                    fiveMinBuyCount+=1
                    fiveMinBuyAvg+=info[2]
                if (int(time.time() * 1000) / 1000 - info[0]) < 1200:
                    tenMinBuyCount+=1
                    tenMinBuyAvg+=info[2]
            else:
                allTimeSellCount+=1
                allTimeSellAvg+=info[2]
                if (int(time.time() * 1000) / 1000 - info[0]) < 600:
                    fiveMinSellCount+=1
                    fiveMinSellAvg+=info[2]
                if (int(time.time() * 1000) / 1000 - info[0]) < 1200:
                    tenMinSellCount+=1
                    tenMinSellAvg+=info[2]
            btctousd+=info[1]
        btctousd/=len(infoArray)
        self.printout(btctousd, allTimeBuyCount, allTimeBuyAvg, allTimeSellCount, allTimeSellAvg, fiveMinBuyCount, fiveMinBuyAvg, fiveMinSellCount, fiveMinSellAvg, tenMinBuyCount, tenMinBuyAvg, tenMinSellCount, tenMinSellAvg)
        return
        
    def printout(self, btctousd, alltimebuy, alltimebuyavg, alltimesell, alltimesellavg, fiveMinbuy, fiveminbuyavg, fiveMinsell, fiveminsellavg, tenminbuy, tenminbuyavg, tenminsell, tenminsellavg):
        print "== Last 5 min orders =="
        print " Buy:", str(fiveMinbuy), "Total amount:", str(fiveminbuyavg), "BTC"
        print "Sell:", str(fiveMinsell), "Total amount:", str(fiveminsellavg), "USD"
        print "== Last 10 min orders =="
        print " Buy:", str(tenminbuy), "Total amount:", str(tenminbuyavg), "BTC"
        print "Sell:", str(tenminsell), "Total amount:", str(tenminsellavg), "USD"
        print "== All-time orders =="
        print " Buy orders:", str(alltimebuy), "Total amount:", str(alltimebuyavg), "BTC @ $"+str(round(btctousd, 3))
        print "Sell orders:", str(alltimesell), "Total amount:", str(alltimesellavg), "USD"
        
class getData(object):
    def alltx(self):
        # TODO run every transfer
        while True:
            time.sleep(5)
            for url in urls:
                try:
                    url_conn = urllib2.urlopen(url)
                    data = json.load(url_conn)
                    #print data
                    #print "_____________________________________________"
                    self.saveData(url[24:31], data)
                except urllib2.HTTPError:
                    pass
                except socket.error, e:
                    pass
                except urllib2.URLError:
                    pass
                time.sleep(5)
        
    def btc_usd(self):
        while True:
            time.sleep(5)
            btc_usd_conn = urllib2.urlopen(btc_usd_url)
            data = json.load(btc_usd_conn)
            #print data
            #print "_____________________________________________"
            self.saveData("btc_usd", data)
            
            
    def ltc_btc(self):
        while True:
            time.sleep(5)
            ltc_btc_conn = urllib2.urlopen(ltc_btc_url)
            data = json.load(ltc_btc_conn)
            #print data
            #print "_____________________________________________"
            self.saveData("ltc_btc", data)
            
    def saveData(self, table, data):
        if table == "btc_usd":
            tidArray = c.execute("SELECT tid from btc_usd").fetchall()
            for x in xrange(len(tidArray)):
                tidArray[x] = tidArray[x][0]
            #print tidArray
            for datum in data:
                if datum['tid'] not in tidArray:
                    #print datum
                    c.execute("INSERT INTO btc_usd(date, price, amount, tid, price_currency, item, trade_type) VALUES (?, ?, ?, ?, ?, ?, ?)", [int(datum['date']), float(datum['price']), float(datum['amount']), int(datum['tid']), str(datum['price_currency']), str(datum['item']), str(datum['trade_type'])])
                    #print "DONE"
                    conn.commit()
        elif table == "ltc_btc":
            tidArray = c.execute("SELECT tid from ltc_btc").fetchall()
            for x in xrange(len(tidArray)):
                tidArray[x] = tidArray[x][0]
            #print tidArray
            for datum in data:
                if datum['tid'] not in tidArray:
                    #print datum
                    #print datum['amount']
                    c.execute("INSERT INTO ltc_btc(date, price, amount, tid, price_currency, item, trade_type) VALUES (?, ?, ?, ?, ?, ?, ?)", [int(datum['date']), float(datum['price']), float(datum['amount']), int(datum['tid']), str(datum['price_currency']), str(datum['item']), str(datum['trade_type'])])
                    #print "DONE"
                    conn.commit()
        elif table == "ltc_usd":
            "test"
        conn.commit()

def help():
    print
    print "Data Options:"
    print "    btc_usd - Bitcoins to USD data"
    print "    ltc_btc - Litecoins to Bitcoins"
    print

def ask():
    while True:
        data = raw_input("Choose data [btc_usd]: ")
        if data == "":
            PrintData().btc_usd("btc_usd")
        elif data == "btc_usd":
            PrintData().btc_usd(data)
        elif data == "ltc_btc":
            PrintData().btc_usd(data)
        elif data == "help":
            help()
        elif data == "reload":
            reload()
        else:
            help()

if __name__ == "__main__":
    conn = sqlite3.connect('./.trades.db')
    c = conn.cursor()
    try:
        c.execute("SELECT * FROM btc_usd")
    except sqlite3.OperationalError, e:
        c.execute('''CREATE table btc_usd (
                     date INT,
                     price FLOAT,
                     amount FLOAT,
                     tid INT PRIMARY KEY,
                     price_currency TEXT,
                     item TEXT,
                     trade_type TEXT)''')

        c.execute('''CREATE table ltc_btc (
                     date INT,
                     price FLOAT,
                     amount FLOAT,
                     tid INT PRIMARY KEY,
                     price_currency TEXT,
                     item TEXT,
                     trade_type TEXT)''')
    try:
        a = threading.Thread(target=ask)
        a.daemon = True
        a.start()
        #print "Ask thread started!"
        g = threading.Thread(target=getData().alltx())
        g.daemon = True
        g.start()
        #print "BTC_USD thread started!"
        #ltc = threading.Thread(target=getData.alltx())
        #ltc.daemon = True
        #ltc.start()
        #print "All threads started!"
    except KeyboardInterrupt:
        print ""
        sys.exit(0)

