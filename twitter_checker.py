import time
import urllib2
import feedparser
import sqlite3
import sys

rssUrl = "https://api.twitter.com/1/statuses/user_timeline.rss?screen_name="
users = ['fontase',
         'cryptopumps',
         'coinmover']

class TwitterChecker(object):
    def parse(self):
        conn = sqlite3.connect('.twitter.db', timeout=45)
        c = conn.cursor()
        for user in users:
            d = feedparser.parse(rssUrl+user)
            userArray = []
            for item in c.execute("SELECT name FROM user").fetchall():
                userArray.append(item[0])
            if user not in userArray:
                c.execute("INSERT INTO user (id, name, messageIDs) VALUES (NULL, :name, :ids)", {'name':str(user), 'ids':"[]"})
                conn.commit()
            for entry in d['entries']:
                epochTime = int(time.mktime(entry.published_parsed))
                #print epochTime, date, entry['title_detail']['value']
                #print user
                test = c.execute("SELECT time from message WHERE (time = :time AND message = :message)", {'time':epochTime, 'message':entry['title_detail']['value']}).fetchall()
                if not test:
                    c.execute("INSERT INTO message (id, time, message) VALUES (NULL, :time, :message)", {'time':epochTime, 'message':entry['title_detail']['value']})
                    conn.commit()
                    latestID = c.execute("SELECT id FROM message WHERE (message.time = :time AND message.message = :message) LIMIT 1", {'time':epochTime, 'message':entry['title_detail']['value']}).fetchone()[0]
                    #print latestID
                    currIDList = eval(c.execute("SELECT messageIDs FROM user WHERE (name = :user)", {'user':user}).fetchone()[0])
                    #print type(currIDList)
                    currIDList.append(latestID)
                    #print currIDList
                    c.execute("UPDATE user SET messageIDs = :IDList WHERE (name = :user)", {'IDList':str(currIDList), 'user':user})
                    conn.commit()
                else:
                    #print "Already in database!"
                    pass
                
if __name__ == '__main__':
    conn = sqlite3.connect('.twitter.db')
    c = conn.cursor()
    try:
        c.execute("SELECT * from user").fetchone()
    except Exception, e:
        c.execute('''CREATE table user (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    name TEXT,
                    messageIDs BLOB)''')
        c.execute('''CREATE table message (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    time INTEGER,
                    message TEXT)''')
    t = TwitterChecker()
    while True:
        t.parse()
        time.sleep(600)
