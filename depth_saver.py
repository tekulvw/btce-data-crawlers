import sqlite3
import urllib2
import json
import time

class depthSaver(object):
    
    depthUrls = ["https://btc-e.com/api/2/btc_usd/depth",
                 "https://btc-e.com/api/2/ltc_usd/depth",
                 "https://btc-e.com/api/2/nmc_btc/depth",
                 "https://btc-e.com/api/2/nvc_btc/depth",
                 "https://btc-e.com/api/2/trc_btc/depth",
                 "https://btc-e.com/api/2/ppc_btc/depth",
                 "https://btc-e.com/api/2/ftc_btc/depth"]
    
    def __init__(self):
        self.conn = sqlite3.connect('./.depths.db', timeout=30)
        self.c = self.conn.cursor()
        try:
            self.c.execute("SELECT * FROM btc_usd")
        except sqlite3.OperationalError, e:
            self.c.execute('''CREATE table btc_usd (
                         empty INTEGER PRIMARY KEY AUTOINCREMENT,
                         date INT,
                         price FLOAT,
                         amount FLOAT,
                         trade_type TEXT)''')

            self.c.execute('''CREATE table ltc_btc (
                         empty INTEGER PRIMARY KEY AUTOINCREMENT,
                         date INT,
                         price FLOAT,
                         amount FLOAT,
                         trade_type TEXT)''')
                         
            self.c.execute('''CREATE table ltc_usd (
                         empty INTEGER PRIMARY KEY AUTOINCREMENT,
                         date INT,
                         price FLOAT,
                         amount FLOAT,
                         trade_type TEXT)''')
                         
            self.c.execute('''CREATE table nmc_btc (
                         empty INTEGER PRIMARY KEY AUTOINCREMENT,
                         date INT,
                         price FLOAT,
                         amount FLOAT,
                         trade_type TEXT)''')
                         
            self.c.execute('''CREATE table nvc_btc (
                         empty INTEGER PRIMARY KEY AUTOINCREMENT,
                         date INT,
                         price FLOAT,
                         amount FLOAT,
                         trade_type TEXT)''')
                         
            self.c.execute('''CREATE table trc_btc (
                         empty INTEGER PRIMARY KEY AUTOINCREMENT,
                         date INT,
                         price FLOAT,
                         amount FLOAT,
                         trade_type TEXT)''')
                         
            self.c.execute('''CREATE table ppc_btc (
                         empty INTEGER PRIMARY KEY AUTOINCREMENT,
                         date INT,
                         price FLOAT,
                         amount FLOAT,
                         trade_type TEXT)''')
                         
    def getData(self):
        while True:
            for url in self.depthUrls:
                try:
                    req = urllib2.urlopen(url)
                    data = json.load(req)
                    self.saveData(url, data)
                except urllib2.HTTPError:
                    pass
                time.sleep(2)  
            time.sleep(5)
    
    def saveData(self, url, data):
        table = url[24:31]
        buys = data['bids']
        sells = data['asks']
        for buy in buys:
            if not (buy[0], buy[1], 'bid') in self.c.execute("SELECT price, amount FROM "+table).fetchall():
                self.c.execute("INSERT INTO "+table+"(empty, date, price, amount, trade_type) VALUES (NULL, :date, :price, :amount, :trade_type)", {'date':int(time.time() * 1000/1000), 'price':float(buy[0]), 'amount':float(buy[1]), 'trade_type':"bid"})
            self.conn.commit()
        for sell in sells:
            if not (sell[0], sell[1], 'ask') in self.c.execute("SELECT price, amount FROM "+table).fetchall():
                self.c.execute("INSERT INTO "+table+"(empty, date, price, amount, trade_type) VALUES (NULL, :date, :price, :amount, :trade_type)", {'date':int(time.time() * 1000/1000), 'price':float(sell[0]), 'amount':float(sell[1]), 'trade_type':"ask"})
            self.conn.commit()
        self.conn.commit()
            
if __name__ == '__main__':
    x = depthSaver()
    x.getData()
                         
