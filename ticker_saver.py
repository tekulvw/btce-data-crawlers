import time
import sqlite3
import json
import urllib2

class tickerSaver(object):
    tickerUrls = ["https://btc-e.com/api/2/btc_usd/ticker",
                  "https://btc-e.com/api/2/ltc_usd/ticker",
                  "https://btc-e.com/api/2/nmc_btc/ticker",
                  "https://btc-e.com/api/2/nvc_btc/ticker",
                  "https://btc-e.com/api/2/trc_btc/ticker",
                  "https://btc-e.com/api/2/ppc_btc/ticker",
                  "https://btc-e.com/api/2/ftc_btc/ticker"]
                  
    def __init__(self):
        self.conn = sqlite3.connect('./.ticker.db', timeout=30)
        self.c = self.conn.cursor()
        try:
            self.c.execute("SELECT * FROM btc_usd")
        except sqlite3.OperationalError, e:
            self.c.execute('''CREATE table btc_usd (
                         high FLOAT,
                         low FLOAT,
                         avg FLOAT,
                         vol FLOAT,
                         vol_cur FLOAT,
                         last FLOAT,
                         buy FLOAT,
                         sell FLOAT,
                         time INTEGER PRIMARY KEY)''')

            self.c.execute('''CREATE table ltc_btc (
                         high FLOAT,
                         low FLOAT,
                         avg FLOAT,
                         vol FLOAT,
                         vol_cur FLOAT,
                         last FLOAT,
                         buy FLOAT,
                         sell FLOAT,
                         time INTEGER PRIMARY KEY)''')
                         
            self.c.execute('''CREATE table ltc_usd (
                         high FLOAT,
                         low FLOAT,
                         avg FLOAT,
                         vol FLOAT,
                         vol_cur FLOAT,
                         last FLOAT,
                         buy FLOAT,
                         sell FLOAT,
                         time INTEGER PRIMARY KEY)''')
                         
            self.c.execute('''CREATE table nmc_btc (
                         high FLOAT,
                         low FLOAT,
                         avg FLOAT,
                         vol FLOAT,
                         vol_cur FLOAT,
                         last FLOAT,
                         buy FLOAT,
                         sell FLOAT,
                         time INTEGER PRIMARY KEY)''')
                         
            self.c.execute('''CREATE table nvc_btc (
                         high FLOAT,
                         low FLOAT,
                         avg FLOAT,
                         vol FLOAT,
                         vol_cur FLOAT,
                         last FLOAT,
                         buy FLOAT,
                         sell FLOAT,
                         time INTEGER PRIMARY KEY)''')
                         
            self.c.execute('''CREATE table trc_btc (
                         high FLOAT,
                         low FLOAT,
                         avg FLOAT,
                         vol FLOAT,
                         vol_cur FLOAT,
                         last FLOAT,
                         buy FLOAT,
                         sell FLOAT,
                         time INTEGER PRIMARY KEY)''')
                         
            self.c.execute('''CREATE table ppc_btc (
                         high FLOAT,
                         low FLOAT,
                         avg FLOAT,
                         vol FLOAT,
                         vol_cur FLOAT,
                         last FLOAT,
                         buy FLOAT,
                         sell FLOAT,
                         time INTEGER PRIMARY KEY)''')
                  
    def getData(self):
        while True:
            for url in self.tickerUrls:
                try:
                    req = urllib2.urlopen(url)
                    data = json.load(req)
                    self.saveData(url, data)
                except urllib2.HTTPError, e:
                    pass
                except urllib2.URLError, e:
                    print e
                    pass
            time.sleep(60)
            
    def saveData(self, url, data):
        table = url[24:31]
        #for item in data['ticker']:
        item = data['ticker']
        self.c.execute("INSERT INTO "+table+"(high, low, avg, vol, vol_cur, last, buy, sell, time) VALUES (:high, :low, :avg, :vol, :vol_cur, :last, :buy, :sell, :time)", {'high':float(item['high']), 'low':float(item['low']), 'avg':float(item['avg']), 'vol':float(item['vol']), 'vol_cur':float(item['vol_cur']), 'last':float(item['last']), 'buy':float(item['buy']), 'sell':float(item['sell']), 'time':int(item['server_time'])})
        self.conn.commit()
                
if __name__ == "__main__":
    x = tickerSaver()
    x.getData()
    
