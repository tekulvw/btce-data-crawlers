import smtplib
import urllib2
import json
import time
import datetime
import sys

class SMS(object):
    providerUrl = "http://www.fonefinder.net/findome.php?npa=%s&nxx=%s&thoublock=%s&usaquerytype=Search+by+Number"
    numbers = {number: provider}
            
    def send(self, message, number=None):
        server = smtplib.SMTP( "smtp.gmail.com", 587 )
        server.starttls()
        server.login( 'ipozidev', PASSWORD )
        message = "\n["+str(datetime.datetime.today().strftime("%H:%M:%S"))+"] "+message
        if number:
            server.sendmail(":O", number, message)
            return
        for pnumber in self.numbers.iterkeys():
            server.sendmail('testing', pnumber+"@"+self.numbers[pnumber], message)
        
class TradeWatcher(object):
    #TODO Need to fill in these methods based on some ranking, most likely of probability from 0 to 100% somehow incorporating historical
    #data. Each check should range from 0 to 25
    def __init__(self, table=None):
        self.table = "btc_usd"
        if table is not None:
            self.table = table
    
        self.tradeConn = sqlite3.connect('./.trades.db')
        self.tradeC = self.tradeConn.cursor()
        
        self.depthConn = sqlite3.connect('./.depths.db')
        self.depthC = self.depthConn.cursor()
        
        self.tickerConn = sqlite3.connect('./.ticker.db')
        self.tickerC = self.tickerConn.cursor()
        
        self.currTime = int(time.time() * 1000 / 1000)
        self.halfHour = self.currTime-1800
        self.oneHour = self.currTime-3600
        self.oneDayAgo = self.currTime-86400
        self.oneWeekAgo = self.currTime-604800
        
    def checkDepths(self):
        halfHourDepths = self.depthC.execute("SELECT * FROM "+table+" WHERE (time>:time)", {'time':self.halfHour}).fetchall()
        hourDepths = self.depthC.execute("SELECT * FROM "+table+" WHERE (time>:time)", {'time':self.oneHour}).fetchall()
        return
        
    def checkTicker(self):
        halfHourTicker = self.tickerC.execute("SELECT * FROM "+table+" WHERE (time>:time)", {'time':self.halfHour}).fetchall()
        hourTicker = self.tickerC.execute("SELECT * FROM "+table+" WHERE (time>:time)", {'time':self.oneHour}).fetchall()
        return
        
    def checkTrades(self):
        return
        
    def checkChatBox(self):
        return 12.5
        
    def findMovingAverage(self):
        return
        
if __name__ == '__main__':
    print "Checker started!"
    try:
        while True:
            time.sleep(150)
            TradeWatcher().checkTrades()
            time.sleep(150)
    except KeyboardInterrupt:
        print
        #SMS().send("Shutting down")
        sys.exit(0)
